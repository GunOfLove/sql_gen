//Prefix - int8 out: 12345678, ord out> ordered, rng33 out> random(33), bse33 out> base+random 100+random(33), flt100 out> 0.88, no prefix out> 'line from input', num out> line from input
//Example - const inputFileNames = ["names","ord_names","rng33","int8","num_numbers","nmr_rand_numbers","flt100","bse100"]
const inputFileNames = ["names","ord_names","rng33","int8","num_numbers","nmr_rand_numbers","flt100","bse100"] 
const tableName = "demo"
const outputFileName = tableName + ".sql"
const numOfColumns = inputFileNames.length;
const numOfRows = 4;
const base=100;


const util = require('util');
const fs = require('fs');

const appendFileSync = util.promisify(fs.appendFile);
const readFile = util.promisify(fs.readFile);

function rand(sides = 6) {
    return Math.floor(Math.random() * sides)
}

function normalizeLetters(word){
    word = word.toLowerCase()
    return word.charAt(0).toUpperCase() + word.slice(1)
}

const run = async () => {
    var output_data_begin = "INSERT INTO "+ tableName + " VALUES (";
    var output_data_end = ") ";
    var input_data = [];

    for (var i = 0; i < numOfColumns; i++) {
        if (inputFileNames[i].slice(0,3) === "int" 
            || inputFileNames[i].slice(0,3) === "rng"
            || inputFileNames[i].slice(0,3) === "flt"
            || inputFileNames[i].slice(0,3) === "bse") {
            continue
        }
        const readData = await readFile('./input/'+inputFileNames[i], 'utf8');
        input_data[i] = readData.split(/\r?\n/)
    }
    
    for (var i = 0; i < numOfRows; i++) {
        var writeLine = output_data_begin;
        for (var j = 0; j < numOfColumns; j++) {
            switch (inputFileNames[j].slice(0,3)) {
                case "int":
                    for (var k = 0; k < Number(inputFileNames[j].slice(3,8)); k++) {
                        var temp = rand(9);
                        if (k == 0 && temp == 0) {
                            temp += 1;
                        }
                        writeLine += temp
                    }
                    break;
                case "rng":
                    writeLine += (rand(inputFileNames[j].slice(3,8)) + 1)
                    break;
                case "bse":
                    writeLine += (rand(inputFileNames[j].slice(3,8)) + base)
                    break;
                case "num":
                    writeLine += input_data[j][i]
                    break;
                case "nmr":
                    writeLine += input_data[j][rand(input_data[j].length)]
                    break;
                case "flt":
                    writeLine += (rand(inputFileNames[j].slice(3,9)) /100)
                    break;
                case "ord":
                    writeLine += "'" + normalizeLetters(input_data[j][i] + "'")
                    break;
                default:
                    writeLine += "'" + normalizeLetters(input_data[j][rand(input_data[j].length)] + "'");
            }
            if ((j+1) != numOfColumns){
                writeLine += ',';
            }
        }
        writeLine += output_data_end + "\n";
        const writeData = appendFileSync('./output/'+ outputFileName, writeLine);
    }
    
}

run();